
class Span{
    cont: HTMLSpanElement
    text: string
    constructor(text: string){
        this.cont = document.createElement("span")
        this.text = text
    }

    getSpan(): HTMLSpanElement {
        return this.cont;
    }
}


class Btn{
    text: string
    button: HTMLButtonElement

    constructor(text: string){
        this.text = text
        this.button = document.createElement("button")
        this.setText() 
    }

    setText(){
        this.button.innerHTML = this.text;
    }

    getBtn(){
        return this.button;
    }
}

class Container{
    cont: HTMLDivElement
    id: string
    constructor(id: string){
        this.cont = document.createElement("div")
        this.id = id
        this.cont.setAttribute("id", id)
    }

    setId(id: string){
        this.cont.setAttribute("id", id)
    }

    getContainer(): HTMLDivElement {
        return this.cont;
    }
}

class Img{
    img: HTMLImageElement
    path: string 

    constructor(path: string){
        this.path = path
        this.img = document.createElement("img")
        this.img.setAttribute("src", path)
    }

    setPath(path: string){
        this.img.setAttribute("src", path)
    }

    getImg(): HTMLImageElement {
        return this.img;
    }

}

// ---------------------- Shape -------------------->>>
class Shape{
    context: any

    constructor(context: any){
        this.context = context
    }

    getStrokeShape(x: number, y: number, width: number, height: number, color: any){
            this.context.beginPath();
            this.context.strokeStyle = color;
            this.context.strokeRect(x, y, width, height)
            this.context.stroke()
    }

    getFillShape(x: number, y: number, width: number, height: number){
            this.context.beginPath();
            this.context.fillRect(x, y, width, height)
            this.context.stroke()
    }
}

// ---------------------- Player -------------------->>>
class Player{
    x: number
    y: number
    width: number
    height: number
    left: number
    right: number
    shape: Shape
    shots: any
    color: any

    constructor(x: number, y: number, width: number, height: number, shape: Shape, color: any){
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.left = 0;
        this.right = 3;
        this.shape = shape;
        this.shots = [];
        this.color = color

        window.addEventListener("keydown", (ev) => {
            if(ev.keyCode == 37) this.moveLeft()
            if(ev.keyCode == 39) this.moveRight()
            if(ev.keyCode == 32) this.shotting()
        })
    }

    shotting(){
        const shot = new Shot(this.x + (this.width / 2), this.y, 5, 10, 0, this.shape, "black")
        this.shots.push(shot);
    }

    moveLeft(){
        if(this.left > 0 && this.left < 4){
            this.right++
            this.left--
        
            let speed = 5
            let friction = 0.05
            let test = setInterval(() => {
                this.x = this.x - speed;
                if(speed > friction) speed -= friction; 
                if(speed < 0.1) clearInterval(test)
            }, 10)
        }
    }

    moveRight(){
        if(this.right > 0 && this.right < 4){
            this.left++
            this.right--
        
            let speed = 5
            let friction = 0.05
            let test = setInterval(() => {
                this.x = this.x + speed;
                if(speed > friction) speed -= friction; 
                if(speed < 0.1) clearInterval(test)     
            }, 10)
        }
    }

    update(){
        this.shape.getStrokeShape(this.x, this.y, this.width, this.height, this.color)  
        this.shots.forEach((s: Shot) => {
            if(s.y < -20){
                let index = this.shots.indexOf(s)
                delete this.shots[index];
            }
            s.update();
        })
    }
}

// ---------------------- Shot -------------------->>>

class Shot{
    x: number
    y: number
    width: number
    height: number
    maxDistance: number
    shape: any
    color: any

    constructor(x: number, y: number, width: number, height: number, maxDistance: number, shape: any, color: any){
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.maxDistance = maxDistance;
        this.shape = shape
        this.color = color
    }

    update(){
        this.y -= 2
        this.shape.getStrokeShape(this.x, this.y, this.width, this.height, this.color)
    }
}

// ---------------------- Enemy -------------------->>>

class Enemy{
    x: number
    y: number
    width: number
    height: number
    maxDistance: number
    shape: any
    color: any

    constructor(x: number, y: number, width: number, height: number, maxDistance: number, shape: any, color: any){
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.maxDistance = maxDistance;
        this.shape = shape
        this.color = color
    }

    attacking(){
        let attack = setInterval(() => {
            this.goDown()
            if(this.y > this.maxDistance){
                clearInterval(attack);
            }
        },20)
    }

    goDown(){
        this.y++
    }

    update(){
        this.goDown()
        this.shape.getStrokeShape(this.x, this.y, this.width, this.height, this.color)   
    }
} 

// ---------------------- myCanvas -------------------->>>

class myCanvas{
canvas: HTMLCanvasElement
width: number
height: number

    constructor(canvas: HTMLCanvasElement, width: number, height: number){
        this.canvas = canvas;
        this.canvas.width = width;
        this.canvas.height = height;
        this.width = width;
        this.height = height;   
    }

    getContext(){
        return this.canvas.getContext("2d");
    }
}

// ---------------------- Game -------------------->>>

class Gameplay{
canvas: myCanvas
player: Player
endGame: boolean
enemies: Enemy[]
enemyTimeout: boolean
enemyCount: number
context: any
score: number
lifesCount: number

spanScore: HTMLSpanElement
scoreContainer: HTMLDivElement
lifesContainer: HTMLDivElement
mainContainer: HTMLDivElement
gameplayPanel: HTMLDivElement 
controlPanel: HTMLDivElement
buttonStart: HTMLButtonElement
lifes: HTMLImageElement[]

    constructor(canvas: myCanvas, player: Player){
        this.canvas = canvas
        this.player = player
        this.endGame = true
        this.enemies = []
        this.enemyTimeout = false
        this.context = this.canvas.getContext()
        this.enemyCount = 0;
        this.score = 0;
        this.lifesCount = 2;

        this.scoreContainer = new Container("score-container").getContainer()
        this.lifesContainer = new Container("lifes-container").getContainer()
        this.mainContainer = new Container("main-container").getContainer()
        this.gameplayPanel = new Container("gameplay-panel").getContainer()
        this.controlPanel = new Container("control-panel").getContainer()
        this.buttonStart = new Btn("start game").getBtn()
        this.spanScore = new Span("Score: 0").getSpan()
        this.lifes = [
            new Img("./heart.png").getImg(),
            new Img("./heart.png").getImg(),
            new Img("./heart.png").getImg()
        ]


        this.buttonStart.addEventListener("mousedown", () => {
            this.endGame = false
            this.start()
            this.controlPanel.style.display = "none";
        });

        this.lifes.forEach(l => {
            this.lifesContainer.appendChild(l)
        })

        this.scoreContainer.appendChild(this.spanScore);
        this.gameplayPanel.appendChild(this.lifesContainer);
        this.gameplayPanel.appendChild(this.scoreContainer);
        this.controlPanel.appendChild(this.buttonStart);
        this.mainContainer.appendChild(this.gameplayPanel);
        this.mainContainer.appendChild(this.controlPanel);
        document.getElementsByTagName("body")[0].appendChild(this.mainContainer);
    }

    getEnemy(){
        const width = 20
        const height = 20
        const x = Math.floor(Math.random() * (this.canvas.width * 0.92)) + (this.canvas.width * 0.02)
        const y = -height
        const shape = new Shape(this.context)
        const enemy = new Enemy(x, y, width, height, this.canvas.height, shape, "red")
        this.enemies.push(enemy);
    }

    start(){
       this.spanScore.innerHTML = "Score: 0";
       this.score = 0;
       this.enemyCount = 0;
       this.enemies = [];
       this.lifesCount = 2;
       this.lifes.forEach(l => {
           l.style.display = "block";
       })

       const play = setInterval(() => {
            if(this.endGame) {
                clearInterval(play);
                this.controlPanel.style.display = "flex";
            } 
            else {
                this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
                
                if(!this.enemyTimeout){
                    this.enemyTimeout = true
                    this.getEnemy()
                    setTimeout(() => this.enemyTimeout = false, 1200)
                }  
                
                this.enemies.forEach((e: Enemy) => {
                    if(e.y > this.canvas.height){
                        this.enemyCount++
                        this.lifes[this.lifesCount].style.display = "none";
                        this.lifesCount--

                        if(this.enemyCount > 2){
                            this.endGame = true;
                            this.buttonStart.innerHTML = "play again"
                        }
                        let index = this.enemies.indexOf(e)
                        delete this.enemies[index];
                    }

                    this.player.shots.forEach((s: Shot) => {
                        if(((s.x + s.width) >= e.x && s.x < e.x + e.width) && (s.y < (e.y + e.height))){
                            this.score++
                            this.spanScore.innerHTML = "Score: " + this.score

                            let sIndex = this.player.shots.indexOf(s)
                            delete this.player.shots[sIndex];

                            let eIndex = this.enemies.indexOf(e)
                            delete this.enemies[eIndex];
                        }        
                    })
                    e.update()
                });

                this.player.update()
            }
        }, 20)
    }
}

// ****************************************************** //
// ---------------------- Main Class -------------------- //
// ****************************************************** //

class Main{
static play(){
const c = document.createElement("canvas");
document.getElementsByTagName("body")[0].appendChild(c);

const canvas = new myCanvas(document.getElementsByTagName("canvas")[0],800, 800)
const shapet = new Shape(canvas.getContext())
const player_one = new Player(10, 800 - 40, 20, 20, shapet, "blue");
const game_f = new Gameplay(canvas, player_one)

game_f.start()
}
}

Main.play()
